#!/usr/bin/python
# -*- coding: utf-8 -*-
#
"""
  Nome: slic_parametros.py
  Autor: Kleyton Sartori Leite - Sob orientacao de Willian Paraguassu

  Descricão: Guardar Superpixels selecionados pelo usuário e, tipo .tif

  Como usar:
  $ python slic_parametros.py --imagem imagem_de_teste.png --segmentos 100 --sigma 5

"""

# Importa bibliotecas necessárias

from skimage import segmentation, io  # segementation: para a SLIC
from skimage.future import graph
from matplotlib import pyplot as plt
from skimage.util import img_as_float
from matplotlib.widgets import Slider, Button
import numpy as np
import argparse
import cv2
import os
import pylab
import time

ap = argparse.ArgumentParser()
ap.add_argument(
    "-i", "--imagem", required=False, help="Arquivo com a imagem",
    default="./7D1.1-000009.45.jpg", type=str
    )
ap.add_argument("-b", "--banco", required=False,
    help="Caminho do banco de imagens", default=".", type=str
    )
ap.add_argument(
    "-mse", "--maxsegmentos", required=False,
    help="Número máximo de segmentos", default=1000, type=int
    )
ap.add_argument(
    "-se", "--segmentos", required=False, 
    help="Número aproximado de segmentos", default=800, type=int
    )
ap.add_argument(
    "-si", "--sigma", required=False, help="Sigma", default=3,
    type=int
    )
ap.add_argument(
    "-sc", "--compactness", required=False, 
    help="Higher values makes superpixel shapes more square/cubic",
    default=18.0, type=float
    )
ap.add_argument(
    "-so", "--outline", required=False, 
    help="Deixa borda do superpixel mais larga: 0 ou 1.",
    default=0, type=int
    )


args = vars(ap.parse_args())

# Atualiza os parâmetros com os valores passados na linha de comando ou com
# os defaults

p_maxsegmentos = args["maxsegmentos"]
p_segmentos = args["segmentos"]
p_sigma = args["sigma"]
p_compactness = args["compactness"]
# realca a fronteira
p_outline = None if (args["outline"] == 0) else (1, 1, 0)

# Imagem a ser Segmentada
nome_imagem_completo = args["imagem"]
pasta_banco_imagens = args["banco"]
image = cv2.imread(nome_imagem_completo)

caminho, nome_imagem = os.path.split(nome_imagem_completo)

c_image = image.copy()

ax = pylab.subplot(111)
pylab.title("Guardar superpixeis selecionados")
pylab.subplots_adjust(bottom=0.35)  # Ajusta espaçamento entre as barras

height, width, channels = image.shape
pylab.axis([0, width, 0, height])

classeAtual = 0

# Extrai os superpixels e mostra na tela com contornos
print "Segmentos = %d, Sigma = %d, Compactness = %0.2f e Classe Atual = %d" % (
    p_segmentos, p_sigma, p_compactness, classeAtual)


start_time = time.time()
segments = segmentation.slic(img_as_float(image), n_segments=p_segmentos,
    sigma=p_sigma, compactness=p_compactness)+1
print "--- Tempo Python skikit-image SLIC: %s segundos ---" % (
    time.time()-start_time)

# Imprimi imagem com segmentacao
obj = ax.imshow(segmentation.mark_boundaries(img_as_float(
    cv2.cvtColor(image, cv2.COLOR_BGR2RGB)), segments,
    outline_color=p_outline))

# Criar as barras de rolagem para manipular o número de segmentos e o sigma
slider_segmentos = Slider(pylab.axes([0.25, 0.25, 0.65, 0.03]),
    'Segmentos' , 10, p_maxsegmentos, valinit=p_segmentos, valfmt='%d'
)
slider_sigma = Slider(pylab.axes([0.25, 0.20, 0.65, 0.03]),
    'Sigma' ,1 ,20 , valinit=p_sigma
)
slider_compactness = Slider(pylab.axes([0.25, 0.15, 0.65, 0.03]),
    'Compactness' ,0.01 ,100 , valinit=p_compactness
)
slider_classes = Slider(pylab.axes([0.25, 0.10, 0.65, 0.03]),
    'Classe Atual' ,0 , 2, valinit=classeAtual, valfmt='%d'
)

# Cores usadas para preencher os superpixels com uma cor diferente para cada classe
if(slider_classes == 2):
    slider_classes = 1
cores = [(255, 204, 153), (0, 0, 0)]

# Ponteiro para a janela
fig = pylab.gcf()


# Determina o que fazer quando os valores das barras de rolagem com os parâmetros do SLIC forem alterados
def updateParametros(val):
    global p_segmentos, p_sigma, p_compactness, segments, image

    p_segmentos = int("%d" % (slider_segmentos.val))
    p_sigma = slider_sigma.val
    p_compactness = slider_compactness.val

    start_time = time.time()
    segments = segmentation.slic(img_as_float(image), n_segments=p_segmentos,
        sigma=p_sigma, compactness=p_compactness)+1
    print("--- Tempo Python skikit-image SLIC: %s segundos ---" % (
        time.time() - start_time))
    obj.set_data(segmentation.mark_boundaries(img_as_float(cv2.cvtColor(
        image, cv2.COLOR_BGR2RGB)), segments, outline_color=p_outline))
    plt.draw()


# Determina o que fazer quando os valores das barras de rolagem da Classe Atual for alterado
def updateClasseAtual(val):
    global classeAtual
    classeAtual = int("%d" % (slider_classes.val))
    print "Segments = %d, Sigma = %d, Compactness = %0.2f e Class= %d"%(
        p_segmentos, p_sigma, p_compactness, classeAtual)

 
# Retorna o superpixel com indice informado por parametro
def getSegmento(indice):
    mask = np.zeros(c_image.shape[:2], dtype="uint8")
    mask[segments == indice] = 255
    mask_inv = cv2.bitwise_not(mask)

    segmento = c_image.copy()
    segmento = cv2.bitwise_and(segmento, segmento, mask=mask)

    contours, _ = cv2.findContours(mask,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)[-2:]

    max = -1
    maiorContorno = None
    for cnt in contours:
        if (len(cnt) > max):
            max = len(cnt)
            maiorContorno = cnt

    x,y,w,h = cv2.boundingRect(maiorContorno)
    segmento = segmento[y:y+h, x:x+w]
        
    # descomente a linha abaixo para gerar a imagem do maior quadrado dentro do superpixel
    # segmento = largestSquare(segmento)
    
    return segmento, mask, mask_inv


# Salva um arquivo .tif contendo o superpixel na pasta correspondente a classe
def saveSegmento(segmento, valorSegmento, classeAtual):
    # se aquele segmento já foi marcado antes, apaga marcação do banco de imagem
    for root, dirs, files in os.walk(pasta_banco_imagens):
        for classe in dirs:
            arquivo = pasta_banco_imagens + classe + "/" + nome_imagem + "_%05d" % valorSegmento + '.tif'
            if(os.path.isfile(arquivo)):
                os.remove(arquivo)

    pasta_classe = pasta_banco_imagens + "/" + "classe_%02d" % classeAtual
    arquivo = pasta_classe + "/" + nome_imagem + "_%05d" % valorSegmento + '.tif'
    if not os.path.exists(pasta_classe):
        os.makedirs(pasta_classe)
    cv2.imwrite(arquivo, segmento)


# Atualiza a imagem pintando o superpixel com a cor da classe informada
def updateImagem(mask, mask_inv, classeAtual):
    global image, c_image
    cor_classe = np.zeros((height, width,3), np.uint8)
    cor_classe[:, :] = cores[classeAtual]
    image_classe = cv2.addWeighted(c_image, 0, cor_classe, 10, 0)
    image_classe = cv2.bitwise_and(image_classe, image_classe, mask=mask)
    
    image = cv2.bitwise_and(image, image, mask=mask_inv)
    mask[:] = 255  
    image = cv2.bitwise_or(image, image_classe, mask=mask)
    
    obj.set_data(segmentation.mark_boundaries(img_as_float(cv2.cvtColor(
        image, cv2.COLOR_BGR2RGB)), segments, outline_color=p_outline)
    )

    pylab.draw()  # Apenas associa a função que pinta o segmento (onclick) com o click do mouse


def onclick(event):
    global image, c_image
    if event.xdata != None and event.ydata != None and int(event.ydata) != 0:
        x = int(event.xdata)
        y = int(event.ydata)
        print "classe atual = %d" % (classeAtual)
        print "segmento = %d" % segments[y, x]
        print "x = %d y = %d " % (x, y)        
        valorSegmento = segments[y, x]

        segmento, mask, mask_inv = getSegmento(valorSegmento)
        saveSegmento(segmento, valorSegmento, classeAtual)

        updateImagem(mask, mask_inv, classeAtual)


fig.canvas.mpl_connect('button_press_event', onclick)

# Associa as barras de rolagens com a função update
slider_segmentos.on_changed(updateParametros)
slider_sigma.on_changed(updateParametros)
slider_compactness.on_changed(updateParametros)
slider_classes.on_changed(updateClasseAtual)

plt.show()
